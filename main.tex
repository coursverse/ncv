% !TeX spellcheck = en_US
% !BIB program=biber
\documentclass[oneside]{book}

%% Standalone for images
\usepackage{standalone}

%% Encoding
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc} 


%% Language
\usepackage[english]{babel}


%% Setup
% Margins
\usepackage{geometry}
\geometry{margin=2.5cm}

% Headers
\usepackage{fancyhdr}

\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\nouppercase{\leftmark}}
\fancyhead[R]{\nouppercase{\rightmark}}
\fancyfoot[C]{\thepage}

% Include handlers
\makeatletter
\newif\ifallincluded
\if@partsw
  \allincludedfalse
\else
  \allincludedtrue
\fi
\makeatother

% Month, Year date
\newcommand{\mmyyyy}{\ifcase \month \or January \or February \or March \or April \or May \or June \or July \or August \or September \or October \or November \or December\fi, \number \year} 

% avoid widow and orphans
\widowpenalty9999
\clubpenalty9999

% change fraction for floats on the pages
\renewcommand{\topfraction}{.8}%
\renewcommand{\bottomfraction}{.8}%

% Manage input paths
% https://tex.stackexchange.com/a/44215/7561
\makeatletter
\providecommand*{\input@path}{}
\def\inputpaths#1{\edef\input@path{\input@path{#1}}\graphicspath{{#1}}}
\makeatother

%% Fonts
% improve the typography
\usepackage{microtype}
% Math fonts (fourier) with utopia (erewhon) text fonts
\usepackage{fourier, erewhon}
% Some nice symbols
\usepackage{fontawesome5}


%% Keys 
\usepackage{pgf}


%% Math
% base
\usepackage{amsmath}
% theorems
\usepackage{amsthm}
% intervals
\usepackage{interval}
% general tools
\usepackage{mathtools}
% differentials
\usepackage{commath}


% avoid breaks within inline math
% https://tex.stackexchange.com/a/94397/7561
% https://tex.stackexchange.com/a/14243/7561
\relpenalty=9999
\binoppenalty=9999

% redefining \times operator to be non-breakable
\let\oldtimes\times
\def\times{{\mkern1mu\oldtimes\mkern1mu}}

% Ceil and floor operators
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}

% remark environment
\newtheoremstyle{remark}% name of the style to be used
{}% measure of space to leave above the theorem. E.g.: 3pt
{}% measure of space to leave below the theorem. E.g.: 3pt
{}% name of font to use in the body of the theorem
{}% measure of space to indent
{\bfseries}% name of head font
{. }% punctuation between head and body
{ }% space after theorem head; " " = normal interword space
{}
%\newtheoremstyle{remark}{}{}{}{}{\bfseries}{\smallskip}{\newline}{}
\theoremstyle{remark}
\newtheorem*{remark}{Remark}

% example environmnet
\newtheoremstyle{example}{}{}{\addtolength{\leftskip}{2.5em}}{}{\bfseries}{\smallskip}{\newline}{}
\theoremstyle{example}
\newtheorem{example}{Example}[chapter]

% definition environment
\newtheoremstyle{definition}{}{}{\addtolength{\leftskip}{2.5em}}{}{\bfseries}{.}{\newline}{}
\theoremstyle{definition}
\newtheorem*{definition}{Definition}

% BigO notations
\DeclareRobustCommand{\bigO}{%
  \text{\usefont{OMS}{cmsy}{m}{n}O}%
}

% trace
\DeclareMathOperator{\tr}{tr}



%% Drawings
\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{
  intersections,
  calc,
  backgrounds,
  matrix,
  positioning,
  spy,
}
\usepgfplotslibrary{
  colorbrewer,
  colormaps,
  fillbetween,
}
% set my seed
\pgfmathsetseed{42}

% General setup for plots
\pgfplotsset{
enlargelimits=false,
% make the plots thicker 
every axis plot post/.append style={semithick},
ymajorgrids,
grid style={dashed}, % make the grid dashed
% Minimal plot grid, removes the y-grid in the plot
minimal plot grid/.style={
  y axis line style={opacity=0},
  axis x line*=bottom,
  x axis line style={black},
},
minimal plot grid,
tick label style={
  /pgf/number format/.cd,
  fixed,
  fixed zerofill,
  precision=1,
  /tikz/.cd,
  font=\footnotesize,
},
label style={font=\footnotesize},
legend style={
  font={\footnotesize},
  legend cell align=left,
  draw=none,
},
% More legend positions
/pgfplots/legend pos/south center/.style={/pgfplots/legend style={at={(0.5,0.03)},anchor=south}},
/pgfplots/legend pos/north center/.style={/pgfplots/legend style={at={(0.5,0.97)},anchor=north}},
}



%% Extras
% Additional commands for writing 
\usepackage{xspace}
\makeatletter
\DeclareRobustCommand\onedot{\futurelet\@let@token\@onedot}
\def\@onedot{\ifx\@let@token.\else.\null\fi\xspace}

\def\eg{{e.g}\onedot} \def\Eg{{E.g}\onedot}
\def\ie{{i.e}\onedot} \def\Ie{{I.e}\onedot}
\def\cf{{cf}\onedot} \def\Cf{{Cf}\onedot}
\def\etc{{etc}\onedot} \def\vs{{vs}\onedot}
\def\wrt{w.r.t\onedot} \def\dof{d.o.f\onedot}
\def\etal{{et al}\onedot}
\def\adhoc{{ad hoc}\xspace}
\def\aka{a.k.a\onedot} \def\Aka{A.k.a\onedot}
\makeatother


%% References
\usepackage[
  refsection=chapter, 
  style=numeric-comp, 
  sorting=noneyear,
  backref=true,
  natbib=true, % add citep and citet
  maxbibnames=99, % authors on bibliography
  maxcitenames=2, % authors on citations
  giveninits=true, % abbreviate the authors names, i.e., put only the initials
]{biblatex}
\addbibresource{abrv.bib}% journal list abbreviation
\addbibresource{ncv.bib}

% Sorts by appearance globally and by year within the citation
% https://tex.stackexchange.com/a/55451/7561
\DeclareSortingTemplate{noneyear}{
  \sort{\citeorder}
  \sort{\field{year}}
  \sort{\field{author}}
}

% Change the normal space to a non-breaking one between the names and the reference
% https://tex.stackexchange.com/a/74932/7561
\renewcommand\namelabeldelim{\addnbspace}


%% Quoting
\usepackage{quoting}
\newcommand\bywhom[1]{% the Bourbaki trick
  {\nobreak\hfill\penalty50\hskip1em\null\nobreak
    \hfill\mbox{\normalfont(#1)}%
    \parfillskip=0pt \finalhyphendemerits=0 \par}%
}

\newcommand*{\openquote}
{\tikz[remember picture, overlay, xshift=-4ex, yshift=1ex]%
  \node[color=gray] (OQ) {\faIcon{quote-left}};}

\newenvironment{quoted}[1]
{\def\quoteauthor{\bywhom{#1}}%
  \begin{quoting}[%
  indentfirst=true,
  leftmargin=\parindent,
  rightmargin=\parindent]\openquote\itshape%
}{\quoteauthor\end{quoting}}


%% Colors
\usepackage{xcolor}


%% Figures
\usepackage[labelformat=simple]{subcaption}% note that the format needs to be on the package itself, and not on the caption setup
\captionsetup{font=footnotesize, labelfont={bf}, labelsep=period}
\renewcommand\thesubfigure{(\alph{subfigure})}
\renewcommand\thesubtable{(\alph{subtable})}


%% Lists
\usepackage{enumitem}


%% Units
\usepackage{siunitx}
\sisetup{
  detect-all=true,
  binary-units=true,
}


%% Algorithms
\usepackage[chapter]{algorithm}% this loads the float environment
\usepackage[noend]{algpseudocode}
\captionsetup[algorithm]{font=footnotesize}

\AtBeginEnvironment{algorithmic}{\footnotesize}

\newcommand*\Let[2]{\State #1 $\gets$ #2}

%% Tables
\usepackage{booktabs}
\usepackage{array}
\newcolumntype{M}{>{$}l<{$}}


%% Links
\usepackage{hyperref}
\hypersetup{
  colorlinks,
  linkcolor = red!50!black,
  citecolor = blue!80!black,% using the ICML blue
  urlcolor  = magenta!75!black,
}

\begin{document}

\title{Notes on Computer Vision}
\author{Ad\'in Ram\'irez Rivera}
\date{\mmyyyy}

\frontmatter
\ifallincluded
  \maketitle
\fi
\include{preface}
\ifallincluded
{% Make a nice TOC with black links
  \hypersetup{linkcolor=black}%
  \tableofcontents%
}
\fi
\mainmatter

\include{cv}
\include{image-processing}
\include{feature-descriptors}
\include{geometry}
\include{flow}
\include{tracking}
\include{recognition}
\include{actions}
\include{light-shading}
\include{segmentation}

\appendix
\include{linear-algebra}




\end{document}