# Notes on Computer Vision

A set of notes that I compiled for Computer Vision topics.

## Access it

You can get the files on two flavors:

- [As a whole](https://gitlab.com/coursverse/ncv/-/jobs/artifacts/master/file/ncv.pdf?job=main), or
- [Per Chapter](https://gitlab.com/coursverse/ncv/-/jobs/artifacts/master/browse?job=chapters)