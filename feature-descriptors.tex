% !TeX spellcheck = en_US
\inputpaths{./images/feature-descriptors/}
\chapter{Feature Descriptors}
\label{ch:features}

When we work with images, we may be interested in their contents.  In particular, we may want to identify whether a set of pixels contain the same information (or semantics) than other.  To identify the contents we need a \emph{fingerprint} of the set of pixels in such a way that it represents the contents uniquely.  That way, if we see another image with a set of pixels with the same fingerprint we know that we are in the presence of the same information.  We call these fingerprints \emph{feature descriptors}.

A feature descriptor is a data structure that represents the contents of the image.  These structures have a plethora of representations, and are based on different theories.  Originally, the feature descriptors were designed in a process called feature engineering.  Nowadays, with deep learning, features are learned simultaneously with the task at hand.

In other words, a feature descriptor is a data structure that holds the information of interesting parts of the image that can be part of a single object or come from a set of them.  What is interesting in an image?  Well it depends on the application.  For instance, if we are making decisions regarding humans, we may be interested in finding humans in the images to then encode different information from each of them.  In general, we may be interested in understanding the whole image, and, thus, creating \emph{global descriptors}; or we may be interested in sub-parts of the image, and, thus, creating \emph{local descriptors}.  The latter can be computed sparsely over the image over invariant points, commonly corners, or densely through a grid.  Moreover, these features can be learned, instead of designed, and they are part of the deep learning revolution that is shaping the computer vision methods.

In this chapter, we discuss different ways of extracting information from the images, and encoding it into feature descriptors.  Moreover, we also discuss how to select image's parts of interest, called \emph{keypoints}, since not all of them may be interesting.


\section{Keypoints}

Reliably detecting keypoints is a cornerstone on several Computer Vision applications.  For instance, to compute the camera pose or to infer the geometry of a scene, we need a set of point correspondences between to images to align them.  Similarly, to stitch two images together, we need the set of points that match the content between the images.  Among many other applications.

The advantage of detecting keypoints is that they are robust to clutter and occlusions, since we can perform partial matches.  There are two main approaches to detect keypoints:
\begin{itemize}
  \item \textbf{Detect and match}.  We detect all relevant keypoints in the images independently.  Then, we match them to find correspondences.
  \item \textbf{Correlate and track}.  We search for similarities between the images to find trackable features (using correlation or least squares).
\end{itemize}

A general pipeline that defines the keypoint's detection process is as follows.
\begin{enumerate}
  \item\label{itm:extraction} \textbf{Feature extraction}.  We search the images for interest points (or, in general, locations) that are invariant\footnote{We would like to have total-invariant features.  That is, features that remain constant regardless of the changes in the images.  However, in practice, it is hard to find these points, and then harder to define a robust invariant structure that still encapsulates the required information.  Thus, we need to make trade-off in our descriptions.} between images.  Normally, we settle for corners since they are invariant to most transformations.
  \item\label{itm:description} \textbf{Feature description}.  Once we have the points of interest, we create a fingerprint that defines them.  In some sense, we describe the information on the support region around each keypoint.  These description is what we call a feature descriptor, and ideally should be as invariant as possible.
  \item\label{itm:matching} \textbf{Feature matching}.  Once we have the feature descriptors, we will need to match them against each other.  Each definition should come with a way of efficiently matching between them (something like a comparison operator that gives a degree of similarity).
  \item\label{itm:tracking} \textbf{Feature tracking}.  Alternatively to the feature matching, we can simply locally search from a previous known location for the corresponding features.  This process is called tracking.  We can do this type of operation on videos or when the locations of the features do not vary drastically.
\end{enumerate}
These steps are well defined in the seminal paper by \citet{Lowe2004}, where he introduced the Scale Invariant Feature Transform.


\section{Feature Detection}

Our first step is to define what content is reliable and consistent to use for our applications.  We are searching for texture information that is invariant to as many changes in the images as possible.  Smooth patches or stable texture patterns are not suitable for detection since they provide little to none location information.  The next option is to use lines or borders.  However, they suffer from the aperture problem, \ie, where in the line are we.  Finally, corners present salient information that let us localize uniquely (given a meaningful enough support region).  Fig.~\ref{fig:fd:patches} shows examples of the previous regions.

\begin{figure}[tb]
  \centering
  \input{patches.tex}
  \caption{Texture on an image may contain different levels information.  Smooth or constant textures are difficult to distinguish, \eg, the sky~(b).  Borders are salient~(c), but suffer from aperture problem (\ie, where within that line are we).  Finally, corners are salient and can be identified uniquely within the image~(a, d).  Image ``All Gizah Pyramids''~\citep{Pyramids2019}.}
  \label{fig:fd:patches}
\end{figure}

\subsection{Corners}

\begin{remark}
The corners are more stable for detection due to having two orientations on the gradients.

Let's define the similarity between to patches.  Let $I$ and $J$ be two images.  Their dissimilarity, $D$, is the sum of their squared differences
\begin{equation}
  \label{eq:fd:wssd}
  D(x; u) = \sum_{i \in \mathcal{N}(x)} w(i) \left( I[i+u] - J[i] \right)^2,
\end{equation}
where $x$ is the position on the image we are interested, $u$ is a displacement,\footnote{Note that the displacement~$u$ is defined as a parameter of the dissimilarity (denoted by the semi-colon) instead of an argument.  That way, we can compute different dissimilarities depending on the defined displacement.} $i$ is a position within the neighborhood~$\mathcal{N}(x)$ of $x$, and $w(\cdot)$ is a weighting function.

To evaluate how meaningful a patch is, we do not yet know the other image.  In that case, we can compute the self-similarity (also known as the auto-correlation) by making $I = J$ on our dissimilarity function.  We, then, define the auto-correlation energy as
\begin{equation}
  \label{eq:fd:ac}
  E_\text{AC}(x;u) = \sum_{i \in \mathcal{N}(x)} w(i) \left( I[i+u] - I[i] \right)^2.
\end{equation}

We can compute this function for small displacement~$u$.  In that case, we can think of $u$ as its differential~$\Delta u$.  Thinking in terms of the differential for the displacement is helpful since we can use a Taylor series expansion~\cite{Lucas1981, Shi1994} to approximate the image, \ie,
\begin{equation}
  \label{eq:fd:taylor-i}
  I(x + \Delta u) \approx I(x) + \nabla I(x) \cdot \Delta u.
\end{equation}
By plugging our approximation~(\ref{eq:fd:taylor-i}) into the auto-correlation~(\ref{eq:fd:ac}), we obtain the surface
\begin{align}
  E_\text{AC}(x; \Delta u) &= \sum_{i \in \mathcal{N}(x)} w(i) \left( I[i+ \Delta u] - I[i] \right)^2, \\
  &\approx \sum_{i \in \mathcal{N}(x)} w(i) \left( I[i] + \nabla I[i] \cdot \Delta u - I[i] \right)^2, \\
  &= \sum_{i \in \mathcal{N}(x)} w(i) \left( \nabla I[i] \cdot \Delta u \right)^2, \\
  &= \Delta u^T A \Delta u,
\end{align}
where
\begin{equation}
  \label{eq:fd:nabla-i}
  \nabla I[i] = \left( \pd{I}{x}, \pd{I}{y} \right) [i]
\end{equation}
is the image gradient\footnote{The gradient of an image can be computed using a derivative kernel and then convolving it with the image.  \Eg, we can use a Sobel~\cite{Pingle1969, Sobel2015}, Kirsch~\cite{Kirsch1971}, Robert~\cite{Roberts1963}, or Prewitt~\cite{Prewitt1970} kernels to extract different types of gradients.} at position $i$, and the auto-correlation matrix~$A$ is defined as
\begin{equation}
  A = W * 
  \begin{bmatrix}
  I_x^2 & I_x I_y \\
  I_x I_y & I_y^2
  \end{bmatrix},
\end{equation}
where the weighted summation was replaced by the convolution with a weight kernel~$W$, and the $I_{(\cdot)}$ are the different components of the derivative of $I$~(\ref{eq:fd:nabla-i}).  The linear combination of the derivatives is also referred to as the \emph{structure tensor}.
\end{remark}

This auto-correlation matrix is important since it allow us to do an analysis of the suitability of the local features without needing to inspect them qualitatively.  The best method is to analyze the eigenvalues of the auto-correlation matrix to discover whether we are in the presence of a corner. \citeauthor{Anandan1985}~\cite{Anandan1985, Anandan1989} showed that the inverse of~$A$ provides a lower bound on the patch's location's uncertainty.  \citet{Foerstner1986, Foerstner1994, Harris1988} were the first ones to use measures derived from the auto-correlation matrix to locate keypoints in the images.

\begin{figure}[tb]
  \centering
  \begin{subfigure}[b]{.5\linewidth}%
    \includegraphics[width=\linewidth]{harris.tex}%
    \caption{$\lambda_0 \lambda_1 - \alpha (\lambda_0 + \lambda_1)^2$}
    \label{fig:fd:harris}
  \end{subfigure}%
  \begin{subfigure}[b]{.5\linewidth}%
    \includegraphics[width=\linewidth]{brown.tex}%
    \caption{$\lambda_0 \lambda_1 / (\lambda_0 + \lambda_1)$}
    \label{fig:fd:brown}
  \end{subfigure}
  \begin{subfigure}[b]{.5\linewidth}%
    \includegraphics[width=\linewidth]{shi.tex}%
    \caption{$\min(\lambda_0, \lambda_1) - T_{\lambda}$}
    \label{fig:fd:shi}
  \end{subfigure}%
  \begin{subfigure}[b]{.5\linewidth}%
    \includegraphics[width=\linewidth]{triggs.tex}%
    \caption{$\min(\lambda_0, \lambda_1) - \alpha \max(\lambda_0, \lambda_1)$}
    \label{fig:fd:triggs}
  \end{subfigure}%
  \caption{(a)~Contours of the F\"orstner-Harris corner response~(\ref{eq:fd:harris}) (with $\alpha=0.06$) where the values of the response define three regions that correspond to the type of patch.  Namely, we have edges when one of the eigenvalues is high and the other low, a flat region when both are low, and corners when both are high.  (b)~Contours of the harmonic mean response~(\ref{eq:fd:harmonic}) that are more pronounced than the previous one.  (c)~Contours of a simpler response that thresholds (with $T_\lambda = 1$) the minimum values~(\ref{eq:fd:shi}).  (d)~Contours of a relaxed approximation of the threshold~(\ref{eq:fd:triggs}) with $\alpha=0.05$.}
  \label{fig:fd:response-contours}
\end{figure}

Similarly, \citet{Harris1988} proposed to use the corner response
\begin{equation}
  \label{eq:fd:harris}
  R = \det(A) - \alpha \tr(A)^2 = \lambda_0 \lambda_1 - \alpha (\lambda_0 + \lambda_1)^2,
\end{equation}
where $\lambda_0$ and $\lambda_1$ are the eigenvalues of~$A$, and $\alpha=0.06$ is a weight trade-off value.  This has the advantage that it does not requires square roots (nor the explicit eigenvalues), while still down-weighting edge-like features where $\lambda_{\max} \gg \lambda_{\min}$.  \citeauthor{Harris1988} determined three cases for the interaction of the eigenvalues:
\begin{itemize}
  \item If both eigenvalues are small, so that the local auto-correlation is flat, then the regions is approximately constant too.
  \item If one eigenvalue is high and the other low, so that the local auto-correlation is a ridge, then the regions is an edge.
  \item If both eigenvalues are high, then the local auto-correlation is sharply peaked which indicates a corner.
\end{itemize}
Fig.~\ref{fig:fd:harris} shows the iso-response contours of the corner response, and where the different regions appear.  Similarly, \citet{Brown2005} proposed to use the harmonic mean as the response
\begin{equation}
\label{eq:fd:harmonic}
R = \frac{\det(A)}{\tr(A)} = \frac{\lambda_0 \lambda_1}{\lambda_0 + \lambda_1}
\end{equation}
that is smoother when $\lambda_0 \approx \lambda_1$.  Note the drastic scale changes, \cf Fig.~\ref{fig:fd:brown}, in comparison with the F\"orstner-Harris response.  \citeauthor{Brown2005} suggest to threshold the responses at~$10$.

\citet{Shi1994}, in the context of image motion, found that the smaller eigenvalue of the matrix suffice to determine good features to track.  They, simply check 
\begin{equation}
\label{eq:fd:shi}
\min(\lambda_0, \lambda_1) > T_\lambda,
\end{equation}
where $T_\lambda$ is a defined threshold.  They response is simpler and constant, \cf Fig.~\ref{fig:fd:shi}.  \citet{Triggs2004} proposed a similar response that constraints the minimum with an amortized maximum, through
\begin{equation}
\label{eq:fd:triggs}
R = \lambda_{\min} - \alpha \lambda_{\max},
\end{equation}
with $\alpha = 0.05$.  Note the similarity with the constant response of \citeauthor{Shi1994} in Fig.~\ref{fig:fd:triggs}.


\subsection{Corners Invariance and Covariance}

For the corners, we want to select positions of the image that are invariant to photometric transformations, and covariant to geometric ones.  In other words, we want to obtain the same points regardless of what transformations happens on the image regarding the illumination and colors of the scene.  Simultaneously, we want to obtain the same changes on the locations as happen with the image, \ie, if the image shifted some amount to the left, we want our keypoints to be shifted the same amount to the left too.

Commonly, we would work with (simple) linear transformations, such as $\hat{I} = a I + b$~\eqref{eq:ip:cont-brigh}, to model the photometric changes.  In this case, we see that the gradient\footnote{Remember that we rely on the gradient as our source of information for the corner extraction.  Hence, it is relevant to understand its invariances and covariances.} is \emph{partially invariant to the intensity shifts} while sensitive to the scale changes, since $\dif \hat{I} = a \dif I$.  

Moreover, note that the derivatives and the weighting function are invariant to shifts.  Consequently, our responses won't be affected by shifts, only the image will be.  Then, our corners are \emph{covariant to translations}.  Similarly, for rotations, our auto-correlation matrix rotates but the eigenvalues remains.  Hence, the localization is also \emph{covariant to rotations}. 

Differently from translations and rotations, scaling changes the neighborhoods.  Hence, the window's size changes to compute both the gradient and the weighting function.  Due to these changes our corner are \emph{not covariant to scale changes}.  Notice that the lack of covariance does not imply the invariance to scaling.

\subsection{Learned Keypoints}

The classic methods rely on corners as salient points to build descriptors around them.  In the deep learning framework, data can be used to learn salient points.  It is common to use labeled datasets of a specific task (\eg, faces, hands, or some salient points) and learn a heat map that corresponds to the interest points in the data.

For instance, \citet{Simon2017} learned hand points in a multi-view setup to improve the 3D localization of the interest points.  Similarly, there has been a push towards facial landmark detection~\cite{Bulat2017, Robinson2019} in which heatmaps are inferred using a supervised training framework.  These works could be extended to other domains by having the correct data.  However, as they are it is hard (if not impossible) to generalize them to other tasks.

An interesting work is MonkeyNet~\cite{Siarohin2019} that learns keypoints to reenact videos, \ie, the method transfers the motion from one video to another one while maintaining the appearance of the latter.  Interestingly, the method relies on finding keypoints (using methods similar to the ones used for landmark detection) to then warp the images.  In their framework, \citeauthor{Siarohin2019} learn keypoints that are salient according to the motion transfer algorithm, instead of the ``edge-based saliency'' we discussed so far.

\citet{Barroso-Laguna2019} use hand-crafted features as data to learn a convolutional neural network that allows them to predict robust and scale independent keypoints.  Moreover, they mixed a multi-view training by posing the keypoint detection problem as a regression of the positions at different views in a hierarchical manner through different quadrants in the images. 

In general, we can see that an hourglass architecture is used to produce heatmaps that are refined later on.  This architecture works by systematically compressing the images into smaller representations, and then using the new representations within an upsampling method that produces the needed heatmaps.  Different ways of regularizing and providing prior knowledge to these heatmaps is what differentiates the methods and their performance.

\section{Feature Descriptors}

\begin{figure}[tb]
\centering
\begin{tikzpicture}[
  spy using outlines={
    circle, 
    Dark2-B,
    magnification=2.0,
    size=2.5cm, 
    connect spies,
  },
]
\node[inner sep=0] (image1)  at (0,0) {\includegraphics[height=4cm]{colosseum-1}};
\coordinate[above right=-.5cm and 1.5cm of image1] (s-1);
\spy on ($(image1.center)+(1,-.5)$) in node at (s-1);
\node[right=1.5cm of image1-|s-1, inner sep=0] (image2) {\includegraphics[height=4cm]{colosseum-2}};
\coordinate[below right=-.5cm and 1.5cm of image1] (s-2);
\spy on ($(image2.center)+(-.5,-.5)$) in node at (s-2);
\end{tikzpicture}
\caption{Two slightly different points of view of the same image.  Interest points (zoomed in) show difference in color, illumination, and geometry, but should be identified and matched uniquely.  The work of the feature descriptors is to create a unique signature that is equivalent in both settings. Images ``Colosseum (Rome)''~\citep{Colosseum2008} and ``Rome, Italy (16318473692)''~\citep{Colosseum2015}.}
\end{figure}

\subsection{Scale Detection}

Because the resolution (frequencies) double at each level, they are called octaves due to an analogy of musical scales where the interval between two pitches doubles its frequency.


\section{Edges and Lines}


\section{Hough Transform}

\section{Model Fitting}

A model is a description of the world.  The model is not necessarily paired with data.  But in the recent years of deep learning, it is more common to have it with it than not.

\printbibliography[heading=subbibliography]