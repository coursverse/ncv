% !TeX spellcheck = en_US
\inputpaths{./images/linear-algebra/}
\chapter{Linear Algebra}
\label{ch:lin-alg}

The following are a set of nuggets of knowledge that intend to familiarize the reader with the minimal information to follow our discussion.  However, they do not intend to cover all the detail of Linear Algebra.  For such purposes, the reader should refer to other references, for instance, the excellent introduction of \citet{Lang1986}.  When working with matrices, reviewing ``The Matrix Cookbook''~\cite{Petersen2012} is highly advised.


\section{Singular Value Decomposition}
\label{sec:svd}

The Singular Value Decomposition (SVD) is a factorization method of a real or complex matrix into three other matrices with particular properties.  For our purposes, we will focus on the real matrices.  SVD is a generalization of the eigendecomposition of a matrix.

Given an $m \times n$ matrix $M$, it is factorized into
\begin{equation}
\label{eq:la:svd}
  M = U \Sigma V^T,
\end{equation}
where $U$ is an $m \times m$ orthogonal matrix,\footnote{An orthogonal matrix $Q$ is a square matrix such that $Q^T Q = Q Q^T = I$.  That is, $Q^T = Q^{-1}$.} $\Sigma$ is an $m \times n$ rectangular diagonal matrix with non-negative real numbers on the diagonal, and $V$ is an $n \times n$ orthogonal matrix.  The diagonal values $\sigma_i$ of $\Sigma$ are the singular values (hence the name) of $M$.  The columns of $U$ and $V$ are the left- and right-singular vectors of $M$, respectively.  Fig.~\ref{fig:la:svd} shows a representation of this process.

\begin{figure}[tb]
  \centering
  \input{svd.tex}
  \caption{Visualization of the matrix $M$ SVD\@.  Note that $U$ has column vectors, while $V^T$ has row vectors.  The singular values on $\Sigma$ represent the rank of matrix $M$.}
  \label{fig:la:svd}
\end{figure}

In the case of square matrices, the operations of these matrices can be thought as rotations and scaling over a space.  We read the operations~(\ref{eq:la:svd}) from right to left.  First, $V^T$ is applied which rotates the axis, then $\Sigma$ scales them, and, finally, $U$ rotates them again.

A geometric interpretation of the SVD is that for every linear mapping,  $T \colon K^m \to K^n$, we can find orthonormal bases of the spaces $K^m$ and $K^n$ such that $T$ maps the $i$th basis of $K^n$ to a non-negative multiple ($\sigma_i$) of the $i$th basis vector of $K^m$, while the reminder of the vectors are zeroed.  Lets see how that happens.

The matrices $U$ and $V$ are orthogonal, and hence unitary.  The columns $U_0$, \dots, $U_{m-1}$ of $U$ form a basis of the space $K^m$, and the columns $V_0$, \dots, $V_{n-1}$ of $V$ form a basis of the space $K^n$.  

The linear transformation $T \colon K^m \to K^n$ is then defined in terms of these bases, such as
\begin{equation}
  T(V_i) = \sigma_i U_i, \qquad i = 0, \dots, \min(m, n)-1,
\end{equation}
where $\sigma_i$ is the corresponding singular value, \ie, the $i$th entry on the diagonal of $\Sigma$, and $T(V_i) = 0$ for $i > \min(m, n)-1$.

Note that if only $r$ singular values are positive, then the matrix $M$ has rank~$r$.\footnote{The rank of a matrix is the maximum number of linearly independent column (or row) vectors in the matrix.}  Then, we can drop the corresponding columns on $U$ and $V$, \ie, the last $m-r$ and $n-r$ columns, respectively, since they will be rank~$r$ too.


\section{Eigenvalue Decomposition}

\begin{definition}[Eigenvalue problem]
The eigenvectors $v_i$, of size $n \times 1$, and the eigenvalues $\lambda_i$ satisfy
\begin{equation}
  A v_i = \lambda_i v_i,
\end{equation}
where $A$ is an $n \times n$ matrix.  That is, the eigenvectors are the vectors that the linear transformation~$A$ scale (either elongate or shrink) by a factor equal to the eigenvalue.
\end{definition}

\begin{definition}[Characteristic polynomial]
The eigenvalue problem yields the characteristic polynomial
\begin{align}
  p(\lambda) &= \det(A - \lambda I) = 0,\\
             &= \lambda^n - g_1 \lambda^{n-1}+g_2 \lambda^{n-2} - \cdots + (-1)^n g_n.
\end{align}
This is an $n$th order polynomial \wrt $\lambda$ with $1 \leq n_\lambda \leq n$ solutions.
\end{definition}

\begin{definition}[Decomposition]
Given an $n \times n$ matrix $A$ with as many eigenvalues as dimensions, \ie, $n_\lambda = n$, then
\begin{equation}
  AV = VD,
\end{equation}
where $V$ is a matrix which column vectors $v_i$ are the eigenvectors, and $D_{ij} = \delta_{ij} \lambda_i$ are scaled eigenvalues.

In the case where $n_\lambda < n$, the Jordan canonical form holds
\begin{equation}
  AV = VJ,
\end{equation}
where $J$ is a block diagonal matrix with the blocks $J_i = \lambda_i I + N$.  The $J_i$ matrices have dimensionality equal to the identical eigenvalues of $\lambda_i$, and $N$ is a square matrix of the same size with $1$ on the super diagonal and zero elsewhere.
\end{definition}

\printbibliography[heading=subbibliography]
