SRC = main
DST = ncv
MK = latexmk
LATEX = --pdf 
FLAGSLATEX = -pdflatex="pdflatex --shell-escape %O %S"
INCLUDEONLY = -pdflatex="pdflatex --shell-escape %O \"\includeonly{$*}\input{%S}\""
INDEX = makeindex

${DST}: aux
	${MK} ${LATEX} ${FLAGSLATEX} ${SRC}.tex -jobname=${DST}

aux: ${SRC}.tex
	${MK} ${LATEX} ${FLAGSLATEX} ${SRC}.tex

index: aux
	${INDEX} ${SRC}

%.aux: aux %.tex
	${MK} ${LATEX} ${INCLUDEONLY} -jobname=the_$* ${SRC}

%.pdf: %.aux
	${MK} ${LATEX} ${INCLUDEONLY} -jobname=the_$* ${SRC}
	mv the_$@ $@

chapters: aux
	for i in *.tex; do \
		if [ "$${i%.*}" != ${SRC} ]; then \
			${MK} ${LATEX} -pdflatex="pdflatex --shell-escape %O \"\includeonly{$${i%.*}}\input{%S}\"" -jobname=the_$${i%.*} ${SRC}; \
			n=$$( sed -n 's/.*\\numberline {\([0-9A-Z]\+\)}.*/\1/p' $${i%.*}.aux ); \
			if [ -z "$${n##[0-9]*}" ]; then \
				suffix=$$(printf "%02d" $${n}); \
			else \
				suffix=$${n}; \
			fi; \
			mv "the_$${i%.*}.pdf" "$${suffix}-$${i%.*}.pdf"; \
		fi; \
	done

clean:
	latexmk -C 
	@rm -f *.log *.aux *.dvi *.toc *.lot *.lof *.bbl *.bcf *.run.xml *.ind *.mtc* *.blg *.idx *.ilg *.maf *.tdo *.gm *.fdb_latexmk *.fls *.out ${SRC}.pdf

veryclean: clean
	@rm -f *.pdf