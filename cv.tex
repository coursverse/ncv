% !TeX spellcheck = en_US
\inputpaths{./images/cv/}
\chapter{Introduction}
\label{ch:intro}

\section{Computer Vision}

Computer Vision is a multidisciplinary field that lies at the intersection of
\begin{itemize}
  \item mathematics, to understand the geometry of the world and to model it, statistical methods and optimization used to solve our models;
  \item neurobiology, to get inspiration from biological vision;
  \item imaging, to get image acquisition;
  \item physics, in particular optics, to understand and model the light sources and its interaction with the world;
  \item signal processing, to further understand the signal that is captured, and later process it;
  \item machine learning, to learn from data different models that fit our observations of the world;
  \item artificial intelligence, to imbue intelligence into our models of the world; and
  \item robotics, to add control to the agents that may be using the machine vision system.
\end{itemize}
The focus of the field is to create algorithms that see.  But, what is ``seeing''?

One definition of ``see'' is
\begin{quoted}{\href{https://www.merriam-webster.com/dictionary/see}{Merrian-Webster}}
1 a: to perceive by the eye.
\end{quoted}
In other words, we are trying to get awareness or understanding of the world through sensors that relay the world's information.

In this sense, computer vision allows computers (and machines) to understand the world through images (of any kind).  Similar to what we, humans, do.  That includes recognizing what is in the world, where it is, how it interacts with its surroundings, and if we want to reach out, to entwine with the cognitive models that hold the representations of such world, and make decisions based on them.

We will be discussing most of the work related to visible light.  However, computer vision's algorithms are not restricted to this spectrum.  On the contrary, computer vision works with any sources of imaging and structured representations.  For instance, medical imaging works with several sources, such as magnetic resonance images, X-rays radiography, ultrasounds, positron emission tomography, among many others.  Other types of sensors include depth, like in autonomous vehicles or in-game consoles; thermal, for heat-based imaging; infrared, for applications like night vision or weather forecasting; and other wavelengths that are used in astronomy.  

\section{Related Fields}
\label{sec:intro:related-fields}

There is an overlap with closer fields, like Image Processing, Computer Graphics, and even Machine Learning (or Pattern Recognition).

Image Processing is interested in low-level tasks, \eg, edge detection, image correction, curve fitting, image scaling, among others.  Similarly, Computer Vision is interested in such tasks, but only as building blocks to create higher representations.  Contrary to Image Processing, Computer Vision is interested in making decisions and learning a representation of the world, while the other is not.

Computer Graphics' objective is to render simulated images that are closer to the real world.  Computer Vision has some intersection on the rendering part, since it may enhance the world around us as a type of action (think about augmented reality).  However, the final goal is not only the rendering but to understand the world to produce and place such renderings.

And similarly, Machine Learning algorithms are interested in learning tasks, just like Computer Vision.  However, Computer Vision is focused on perception and action on the world, while Machine Learning has a wider set of action.

Finally, Machine Vision can be seen as a different field (or as a subfield) of Computer Vision.  However, the main difference resides on that Machine Vision is concerned with acting on the real world through actuators and embodied agents, while the other can actuate through any type of agents.

Another way of seeing their relations is the following:  Image Processing converts images into images, Computer Vision converts images into models, and Computer Graphics converts models into images.  In that sense, throughout our discussions, we will be concerned about how to create models from our inputs.


\section{Not So Brief History}

In the following, we traverse what seem to be the most relevant events on Computer Vision history.  Note that this is not a thorough list, and important events to some may be missing.  Figure~\ref{fig:intro:history} shows a brief history depiction.

\begin{figure}[tb]
  \resizebox{\linewidth}{!}{\input{cv-timeline.tex}}
  \caption{A brief history of Computer Vision.}
  \label{fig:intro:history}
\end{figure}

\begin{itemize}[font=\itshape]
  \item[1943] \citet{McCulloch1943} tries to understand how the brain can produce complex patterns from simple cells connected together.  They proposed a simplified model of a neuron that later was improved by \citet{Rosenblatt1958}.
  
  \item[1948] \citet{Wiener1948} lays the foundation for several fields by introducing a theoretical foundation of self-regulating systems.  It provides a startup for automatic navigation, artificial intelligence, neuroscience, and communications.  
  
  \item[1949] \citet{Hebb1949} produces the first comprehensive theory about how brain activity might produce various complex psychological phenomena.  Several fields emerge, and there is an transdisciplinary research boom.
  
  \item[1950] \citet{Gibson1950} introduces the idea of optical flow for human perception. 
  
  \citet{Turing1950} introduces the ``imitation game'' (commonly known as the Turing Test).
  
  \item[1956] Dartmouth workshop on AI\@.
  
  \item[1957]  The first image\footnote{It was the photo of Kirsch's son. NIST timeline: \url{https://www.nist.gov/timeline\#event-774341}.} is digitized by Russell Kirsch and colleagues at NIST\@.
  
  \item[1958] \citet{Rosenblatt1958} introduces the idea of the perceptron (an electronic brain that teaches itself).  (Not related yet to vision, but it will become relevant in the '80s and 2010 with the introduction and resurrection of Neural Networks.)
  
  First AI hype as the perceptron could be used as first non-human controls, artificial brain, and, more importantly, could learn by doing. % Most of the applications we have today were already imagined then.
  
  \item[1959] \citet{Hubel1959} seminal paper ``Receptive fields of single neurons in the cat's striate cortex'' describes the simple and complex neurons processing on the visual cortex.  That is, our vision process does not start with holistic objects.
  
  In particle physics~\citep{Hough1959}, the Hough Transform was used to analyze bubble chamber photos.  The process was simple for today's standards.  It consisted in extracting lines and dividing the image into framelets for posterior analysis.  However, it started several lines of research that are still in pursuit today.
  
  \item[1960] Julesz and colleagues~\cite{Julesz1960,Julesz1961,Julesz1962,Julesz1973} starts generating stochastic textures, and studies how the human perception recognizes them.  They use these experiences to support texture discrimination systems based on local feature extractors and aggregated by a global processor based on statistics.
  
  \citet{Kelley1960} uses gradient theory (a precursor of back-propagation) for automatic control.
  
  \item[1962] Bryson's group~\cite{Bryson1962} develop back-propagation methods based on gradients (simultaneously with Kelley~\cite{Bryson1969}).
  
  \citet{Dreyfus1962} simplifies the Dynamic Programming-based derivation of back-propagation using the chain rule.
  
  
  \item[1963] Robert's thesis~\citep{Roberts1963} that derives 3D information from 2D images through line extraction and shape inference.  This thesis marks the birth of Computer Vision, according to some.
  
  \item[1964] Bledsoe and colleagues~\citep{Bledsoe1964, Bledsoe1965, Bledsoe1966, Bledsoe1966a} start researching how to recognize human faces.  They faced problems related to the degrees of freedom of the faces.
  
  \item[1966] Papert's Summer Vision project~\citep{VsionProject} attempts to develop a visual system.  (Spoiler Alert)  The project was not successful.  This project marks the birth of Computer Vision as a field, according to some.
  
  Minsky gave one of his undergraduate students the task of linking a camera to the computer and make the computer describe what it sees.  The task proved to be more difficult than expected, though.
  
  \item[1968] \citet{Guzman1968} works on shape-from-contour.  He continued his work on the following years~\citep{Guzman1971}.
  
  \item[1969] Minsky and Sempart' book~\citep{Minsky1969} marks the start of ``AI's winter.''  In this book, they criticize neural networks.\footnote{An interesting reading~\cite{Olazaran1996} about the history of the perceptron controversy.}
  
  \item[1970] As part of Artificial Intelligence, the field focuses on understanding the human vision and emulating human perception.
  
  \citet{Horn1970} study shape from shading.
  
  \citet{Linnainmaa1970} writes the modern efficient version of back-propagation for discrete sparse networks.
  
  \item[1971] \citet{Land1971} propose the Retinex Theory. This theory assumes that there are three independent cone systems, each starting with a set of receptors peaking, respectively, in the long-, middle-, and short-wavelength regions of the visible spectrum.  Color constancy is described as a desired property of computer vision algorithms.
  
  \citet{Binford1971} proposes generalized cylinders as a model for representing visual shape. 
  
  \item[1972] \citet{Duda1973} publish a comprehensive state of pattern classification and scene recognition.
  
  \citet{Waltz1972} studies shape-from-shadows.
  
  \item[1973] ICPR starts.
  
  \citet{Kanade1973} proposes the first complete automated system for face recognition.  The system did feature-based face recognition.  It had special purpose methods to locate eyes, nose, mouth, and boundaries of face.  It computed approximately \num{40} geometric features, \eg, ratios of distances and angles between features.
  
  \citet{Yakimovsky1973} propose a general segmentation algorithm based on Bayesian decision theory.
  
  \item[1974] \cite{Werbos1974} applies back-propagation to nueral networks.
  
  \item[1975] \citet{Koenderink1975} propose the foundation of what will become optical flow.
  
  \item[1976] Kurzweil unveiled the first Optical Character Recognizer, and it started selling them commercially on the following two years.
  
  \citet{Levine1976} develop a system with multi-pyramid images (with different properties) for top-down and bottom-up segmentation.
  
  \citet{Rosenfeld1976} propose an ambiguity-reduction process for objects in a scene through iterative parallel operations (\ie, the relaxation operations) over the data.
  
  \item[1977] \citet{Ullman1977} studies visual motion analysis.  In particular, he focuses in two problems.  The correspondence problem, which concerns the identification of image elements that represent the same object at different times, thereby maintaining the perceptual identity of the object in motion or in change.  And the three-dimensional interpretation of the changing image once a correspondence has been established.
  
  \item[1978] \citet{Barrow1978} propose intrinsic images to extract confounding properties from the light-intensity image.  
  
  \citet{Hanson1978} propose a seminal work on scene recognition.
  
  \citet{Ohta1978} propose a scene analysis system to semantically segment an image into hierarchical structure.
  
  \citet{Tanimoto1978} defines a pyramid as a series of digitizations of the same image at different resolutions.  \citet{Crowley1978} notes problems due to aliasing on the averaging of local features during the pyramid construction.  These pyramids seems to be closer to our ``modern'' definition than the processing cones used before.\footnote{\citet[Section~2.2.4]{Crowley1981} gives an in depth description of previous image pyramid definitions and usage.  \citet{Adelson1984} give a more modern take on image pyramids.}
  
  \item[1979] TPAMI starts.
  
  \citet{Koenderink1979} study the creation of aspect graphs to model different views of objects.  Hence, the observer could create a model of the object that doesn't depend on cognition but rather on the physics of the object.  \citet[Section~6.1.4]{Minsky1975} described a similar concept much earlier.
  
  % '80s Artificial Neural Networks return, and leave the eye of the community.  There are advancements in geometry and math.
  \item[1980] Fukushima proposed Neocognition~\citep{Fukushima1980}, the first (deep) neural network that included (first) convolutional layers based on Hubel and Wiesel ideas.
  
  \item[1981] \citet{Lucas1981} introduce the widely used differential method to compute optical flow.  This method is used as a baseline on several other approaches that depend on motion, like recovering structure from motion, or for localization and mapping.
  
  \citet{Horn1981} introduce a smoothness global constraint to solve the aperture problem to compute optical flow.
  
  \citet{Fischler1981} propose the seminal RANSAC paradigm for fitting models in the presence of noisy data.
  
  \citet{Longuet-Higgins1981} proposes an algorithm for estimating the essential matrix from a set of corresponding normalized image coordinates and for determining the relative position and orientation of the two cameras (given that the matrix is known).  \citeauthor{Longuet-Higgins1981} shows how the 3D coordinates of the image points can be determined with the aid of the essential matrix. 
  
  \citet{Julesz1981} discovers that textures are processed in the human-visual system through the first-order statistics.  Moreover, to perceive the phase of the texture, one needs to pay focal attention.
  
  \item[1982] Marr's influential paper~\citep{Marr1982} built on top of Hubel and Wiesel's work, and established that our visual system is hierarchical.  And its objective is to infer 3D representations of the environment to interact with.  Similar to Robert's thesis~\citep{Roberts1963}, Marr's framework introduces low-level features (\eg, edges, curves, corners, \etc) to build higher-level representations, that go to 2.5D (\ie, 2D plus depth), and then to 3D.
  
  \item[1983] First CVPR\@.
  
  \citet{Witkin1983} proposes the scale-space filtering as an alternative to pyramids where a signal is described in a multi-scale structure.
  
  \citet{Faugeras1983} propose a 3D recognition method by matching primitive geometrical surfaces.
  
  \item[1984] \citet{Geman1984} make an analogy between images and statistical mechanics systems.  In particular, they explore the Markov Random Field model to represent the dependencies among the pixels' intensities.
  
  \citet{Koenderink1984} provides a theoretical foundation for multi-resolution processing by embedding the image into a family of images parameterized by the scale through a diffusion process.
  
  \citet{Grimson1984} propose a way to recognize 3D shapes by local measurements through robots.
  
  \item[1985] \citeauthor{Bajcsy1985}~\cite{Bajcsy1988, Bajcsy1985} consolidates previous attempts to do active vision.  His proposal expands the ideas of active sensing into active perception.
  
  \item[1986] \citet{Canny1986} proposed his method to detect multiple edges on images and a theory behind the responses of edges on images.  His method is still widely used as a basis to extract borders for further processing.
  
  \citet{Plantinga1986} propose a method to compute the aspect graph of an object.
  
  \citet{Rumelhart1986} propose their method for learning representations with back-propagation.
  
  \item[1987] First ICCV and IJCV\@.
  
  \citet{Rumelhart1987} create computer simulations of perceptron, giving to computer scientists their first testable models of neural processing.
  
  \citet{Sirovich1987} showed that Principal Component Analysis can be used on a set of faces to form a basis of the features.  
  
  \citet{Kass1987} propose the seminal active contours (\aka snakes) to do segmentation by energy minimization.
  
  \item[1988] \citet{Harris1988} propose a corner and edge detector based on the auto-correlation function of local patches.
  
  \citet{Dickmanns1988} proposes a dynamic scene analysis (active vision) that combines 3D shape models, dynamical models (known from modern control theory) and the laws of perspective projection.
  
  \citet{Bergen1988} show that early vision filter responses can explain a lot.
  
  \item[1989] \citet{LeCun1989} show progress using Neural Networks by learning the convolution kernel coefficients through backpropagation and proposed an architecture for hand-written digit recognition.  Most of his work spans from ideas of \citet{Fukushima1980}.
  
  \citet{Mumford1989} propose their functional, \ie, an optimality criterion for segmenting an image into sub-regions.
  
  \citet{Ballard1989} explores active vision as a simpler task in animate systems that can control, not only their gaze, but also other control mechanisms (on self and the environment) that may ease the task.
  
  \item[1990] The field is established separately from Artificial Intelligence.
  
  \citet{Gigus1990} propose a method to construct the aspect graph of an object from their lines.
  
  \citet{Malik1990} use multi-scale and -orientation to understand textures.
  
  There is a major focus on geometric approaches, face recognition, and statistical analysis.
  
  \item[1991] \citet{Turk1991} proposed the Eigenfaces method that builds on top of Sirovich and Kirby's ideas.  The Computer Vision algorithms start learning from data instead of being designed.
  
  \item[1992] \citet{Faugeras1992} have a panel discussing the aspect graphs and their stagnation.  The aspect graphs haven't become a wide spread structure to represent the objects.
  
  \citet{Hartley1992, Faugeras1992a} simultaneously derive the Fundamental matrix.  Similarly to the Essential matrix, the former models the relation between the projective geometry, while the latter contains information of the cameras involved.
  
  \citet{Mundy1992} compile a set of papers dealing with geometric invariants and their application to Computer Vision.
  
  \citet{Boser1992} proposes what will become Support Vector Machines.  This idea gets refine by Vapnik and colleagues over the years~\cite{Vapnik1995, Cortes1995}.
  
  \item[1993] \citet{Black1993} propose a framework based on robust estimation that addresses violations of the brightness constancy and spatial smoothness assumptions caused by multiple (optical flow) motions. 
  
  \item[1995] \citet{Murase1995} learn an eigenspace (appearance-based model) to recognize 3D objects.
  
  \item[1996] \citet{Schmid1996} propose a keypoint-based image indexing algorithm.
  
  \citet{Rowley1996} proposes a neural network to do face detection.  Their work was summarized later on but it builds on the same principles~\citep{Rowley1998, Rowley1998a}.
  
  \item[1997] \citet{Shi1997} proposed the novel graph-based segmentation.  Thus, birthing new approaches to do segmentation based on inter- and intra-features.
  
  \citet{Hartley1997} proposes the normalized eight-point algorithm that supersedes the method introduced by \citet{Longuet-Higgins1981}.
  
  \citet{Torr1997} propose the use of RANSAC for robust fundamental matrix estimation.
  
  \citet{Osuna1997} present a decomposition algorithm that can be used to train SVM's over large datasets.
  
  \item[1998] LeNet-5~\citep{LeCun1998} dominated the hand-digit recognition, and used end-to-end learning.  Thus, it learned the features and the classifier simultaneously.
  
  \citet{Burl1998} create probabilistic deformable models that learn to recognize object.
  
  \citet{Schneiderman1998} proposes an algorithm for object recognition that explicitly models and estimates the posterior probability function.  They learn the needed distributions by counting.
  
  \citet{Bregler1998} introduce the product of exponential maps and twist motions, and its integration into a differential motion estimation framework to track people.
  
  \citet{Isard1998} propose a factored sampling to model multi-modal distributions.  Their model uses learned dynamical models, together with visual observations, to propagate the random set over time.
  
  \item[1999] Shift into feature-based object recognition, instead of Marr's hierarchical models.
  
  Lowe proposes Scale Invariant Feature Transform~\citep{Lowe1999}, which starts a new era of local feature descriptors.
  
  \citet{Triggs1999} consolidates bundle adjustment theory from photogrammetry and geodesy literature to the Computer Vision community.
  
  \citet{Boykov1999} use graph cuts to compute a local minimum even when very large moves are allowed.  Their solution is within a known factor of the global minima.  They expanded their work later on~\cite{Boykov2001}.
  
  \citet{Stauffer1999} use Mixture of Gaussians to model the information of pixels to model the background of scenes for real-time tracking.
  
  \item[2000] \citet{Hartley2000} publish their seminal book addressing multi-view geometry.
  
  \citet{Weber2000} propose a method to learn object class models (represented as flexible constellations of rigid parts, \ie, features) from unlabeled and unsegmented cluttered scenes for the purpose of visual object recognition.
  
  \citet{Comaniciu2000} use mean shift iterations to find the most probable target for real-time tracking of non-rigid objects.
  
  \citet{Sidenbladh2000} define a generative model of the image, a likelihood function based on intensity, and a prior probability distribution over pose and joint angles that allows them to track 3D humans.
  
  \item[2001] Viola and Jones proposed the first real-time face detector using Haar-like features and cascade classifiers~\citep{Viola2001}.  The same approach was generalized for objects too~\citep{Viola2001a}.
  
  \citet{Faugeras2001} publish their book on projective structure from motion.
  
  \citet{Kadir2001} propose a keypoint detector that exploits saliency, scale selection, and content description to improve the description since they are related aspects.
  
  \item[2002] \citet{Mikolajczyk2002} propose an scale invariant detector that computes a multi-scale representation for the Harris interest point detector and then selects points at which a local measure (the Laplacian) is maximal over scales.
  
  \citet{Matas2002} create a detector of maximally stable extremal regions.  These regions possess are closed under continuous (and thus projective) transformation of image coordinates and monotonic transformation of image intensities.
  
  \item[2006] Pascal VOC dataset was released as a way to standardize the object classification task.
  
  \citet{Bay2006} introduce the novel scale- and rotation-invariant interest point detector and descriptor: Speeded Up Robust Features.  It is a successor of SIFT\@.
  
  \item[2009] \citet{Felzenszwalb2008} proposed a Deformable Part-based Model that decomposes the objects into parts and learn to detect both (the parts and the object) simultaneously.
  
  \item[2010] % ANNs again, deep learning
  
  ImageNet Large Scale Visual Recognition Competition (ILSVRC) was launched~\citep{Deng2009}, following the steps of Pascal VOC\@.  The principal difference was the sheer amount of classes and data, over a million images and thousand object classes.  And rapidly became a benchmark for object classification and detection.
  
  \item[2012] \citet{Krizhevsky2012} proposed AlexNet, a method based on convolutional neural networks, and achieved an error rate of $16.4\%$, in comparison to the previous methods $26\%$, at the ILSVRC\@.  This paper marked the breakthrough to bring Neural Networks to the spotlight, and shift the community to study their potential in other problems of Computer Vision.
  
  \item[2014] Region-based Convolutional Network (RCNN)~\citep{Girshick2014} was proposed as the first deep learning network to produce bounding boxes, thus going beyond simple class probabilities.  This method, however, is not end-to-end and still relies on Support Vector Machines as classifiers.
  
  VGG-16~\citep{Liu2015} is introduced, and is a front runner at the ILSVRC of that year.  That same competition Goo\-gle\-Net~\citep{Szegedy2015} is introduced and won the ILSVRC\@.
  
  \item[2015] Spatial pyramid pooling~\citep{He2015} was introduced into the deep architectures.  This method enhanced detection for different region detections.
  
  ResNet~\citep{He2016} is introduced and surpasses human performance on ILSVRC of that year.  The introduction of residual blocks helps to propagate the gradient on extremely deep networks, which helped to won the competition.  This idea is further explored by models in the following years.
  
  Girshick made advances towards real-time object detection by advancing his RCNN~\citep{Girshick2015}, named Fast-RCNN.  Simultaneously, \citet{Ren2015} made advances to improve Girshick's new ideas too, and named it Faster-RCNN.
  
  \item[2016] You Only Look Once~\citep{Redmon2016} was proposed by Redmon \etal.  This paper revolutionized the object detection tasks since it allowed to do detection and classification on an end-to-end basis with neural networks.
  
  \item[2017] \citet{He2017} extended the Faster-RCNN method to do instance segmentation (\ie, to distinguish instances of the same objects within the same image).
  
  \item[2018] Photo-realistic generation techniques for images~\cite{Li2018} and videos~\cite{Chan2018, Wang2018}.  
  
  There is a surge of adversarial attacks (and defenses) for several architectures.  Moreover, there were findings that the same attacks may work on humans~\cite{Elsayed2018} and a debate on how to define adversarial images started.
  
  \item[2019] There was an extension of Generative Adversarial Networks to produce high-resolution images~\c  ite{Brock2019}.  
\end{itemize}


\printbibliography[heading=subbibliography]
